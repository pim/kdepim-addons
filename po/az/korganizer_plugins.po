# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdepim-addons package.
#
# Kheyyam Gojayev <xxmn77@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdepim-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-26 00:39+0000\n"
"PO-Revision-Date: 2022-02-17 17:56+0400\n"
"Last-Translator: Kheyyam Gojayev <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#: emailaddressselectionldapdialog/emailaddressselectionldapdialog.cpp:35
#, fuzzy, kde-format
#| msgid "Search &Directory Service"
msgctxt "@action:button"
msgid "Search &Directory Service"
msgstr "Kataloq xi&dməti axtarışı"

#~ msgctxt "@title:window"
#~ msgid "Configure Day Numbers"
#~ msgstr "Tarixin tənzimləmək"

#~ msgid "Show Date Number"
#~ msgstr "Tarixin nömrəsini göstərmək"

#~ msgid "Show day number"
#~ msgstr "Günün nömrəsini göstərmək"

#~ msgid "Show days to end of year"
#~ msgstr "İlin sonunadək günlərin sayı"

#~ msgid "Show both"
#~ msgstr "Hər ikisini göstərmək"

#~ msgid "This plugin shows information on a day's position in the year."
#~ msgstr "Bu plaqin, günün ilin neçənci günü olduğunu göstərir."

#~ msgid "1 day before the end of the year"
#~ msgid_plural "%1 days before the end of the year"
#~ msgstr[0] "İlin sonuna qədər 1 gün"
#~ msgstr[1] "İlin sonuna qədər %1 gün"

#~ msgctxt "dayOfYear / daysTillEndOfYear"
#~ msgid "%1 / %2"
#~ msgstr "%1 / %2"

#~ msgid "1 day since the beginning of the year,\n"
#~ msgid_plural "%1 days since the beginning of the year,\n"
#~ msgstr[0] "İlin başlanğıcından sonra 1 gün,\n"
#~ msgstr[1] "İlin başlanğıcından sonra %1 gün,\n"

#~ msgid "1 day until the end of the year"
#~ msgid_plural "%1 days until the end of the year"
#~ msgstr[0] "İlin sonuna qədər 1 gün"
#~ msgstr[1] "İlin sonuna qədər %1 gün"

#~ msgctxt "Week weekOfYear"
#~ msgid "Week %1"
#~ msgstr "Həftə %1"

#~ msgid "1 week since the beginning of the year"
#~ msgid_plural "%1 weeks since the beginning of the year"
#~ msgstr[0] "İlin başlanğıcından 1 həftə sonra"
#~ msgstr[1] "İlin başlanğıcından %1 həftə sonra"

#~ msgctxt "weekOfYear (year)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgctxt "Week weekOfYear (year)"
#~ msgid "Week %1 (%2)"
#~ msgstr "Həftə %1 (%2)"

#~ msgid "1 week remaining"
#~ msgid_plural "%1 weeks remaining"
#~ msgstr[0] "1 həftə qalır"
#~ msgstr[1] "%1 həftə qalır"

#~ msgid "1 week until the end of the year"
#~ msgid_plural "%1 weeks until the end of the year"
#~ msgstr[0] "İlin sonuna qədər 1 həftə"
#~ msgstr[1] "İlin sonuna qədər %1 həftə"

#~ msgctxt "weekOfYear / weeksTillEndOfYear"
#~ msgid "%1 / %2"
#~ msgstr "%1 / %2"

#~ msgctxt ""
#~ "n weeks since the beginning of the year\n"
#~ "n weeks until the end of the year"
#~ msgid ""
#~ "%1\n"
#~ "%2"
#~ msgstr ""
#~ "%1\n"
#~ "%2"

#~ msgctxt "@title:window"
#~ msgid "Configure Holidays"
#~ msgstr "Bayramlar"

#~ msgid "Use Israeli holidays"
#~ msgstr "İsrail bayramlarını yerləşdirin"

#~ msgid "Show weekly parsha"
#~ msgstr "Tövratın həftəlik başlıqlarını göstərmək"

#~ msgid "Show day of Omer"
#~ msgstr "Omer günü göstərmək"

#~ msgid "Show Chol HaMoed"
#~ msgstr "Xol haMod (həftəarası tətillər) göstərmək"

#~ msgctxt ""
#~ "Change the next two strings if emphasis is done differently in your "
#~ "language."
#~ msgid ""
#~ "<qt><p align=\"center\"><i>\n"
#~ "%1\n"
#~ "</i></p></qt>"
#~ msgstr ""
#~ "<qt><p align=\"center\"><i>\n"
#~ "%1\n"
#~ "</i></p></qt>"

#~ msgid "This plugin provides the date in the Jewish calendar."
#~ msgstr "Bu plaqin yəhudi təqvimindəki tarixi göstərir."

#~ msgctxt ""
#~ "These are Jewish holidays and mostly do not have translations. They may "
#~ "have different spellings in your language; otherwise, just translate the "
#~ "sound to your characters."
#~ msgid "Sh. HaHodesh"
#~ msgstr "Şabbat HaHodeş"

#~ msgid "Erev Pesach"
#~ msgstr "Pesax qanunu"

#~ msgid "Sh. HaGadol"
#~ msgstr "Shabbat HaQadol"

#~ msgid "Pesach"
#~ msgstr "Pesax"

#~ msgid "Chol Hamoed"
#~ msgstr "Xol Hamoed"

#~ msgid "Yom HaShoah"
#~ msgstr "Yom HaShoah"

#~ msgid "Yom HaAtzmaut"
#~ msgstr "Yom HaAtzmaut"

#~ msgid "Yom HaZikaron"
#~ msgstr "Yom HaZikaron"

#~ msgid "Yom Yerushalayim"
#~ msgstr "Yom Yerushalayim"

#~ msgid "Lag BaOmer"
#~ msgstr "Lag BaOmer"

#~ msgid "Erev Shavuot"
#~ msgstr "Shavuot qanunu"

#~ msgid "Shavuot"
#~ msgstr "Shavuot"

#~ msgid "Tzom Tammuz"
#~ msgstr "17 Tammuz orucu"

#~ msgid "Sh. Hazon"
#~ msgstr "Shabbat Hazon"

#~ msgid "Sh. Nahamu"
#~ msgstr "Shabbat Nahamu"

#~ msgid "Tisha B'Av"
#~ msgstr "Tisha B'Av"

#~ msgid "S'lichot"
#~ msgstr "S'lichot"

#~ msgid "Erev R.H."
#~ msgstr "Roş haŞana qanunu"

#~ msgid "Rosh Hashana"
#~ msgstr "Roş Haşana"

#~ msgid "Sh. Shuvah"
#~ msgstr "Shabbat Shuvah"

#~ msgid "Tzom Gedalia"
#~ msgstr "Qedalia orucu"

#~ msgid "Erev Y.K."
#~ msgstr "Tom kipur qanunu"

#~ msgid "Yom Kippur"
#~ msgstr "Yom Kipur"

#~ msgid "Erev Sukkot"
#~ msgstr "Sukkot qanunu"

#~ msgid "Sukkot"
#~ msgstr "Sukkot"

#~ msgid "Hoshana Rabah"
#~ msgstr "Hoşana Rabah"

#~ msgid "Shmini Atzeret"
#~ msgstr "Shmini Atzeret"

#~ msgid "Simchat Torah"
#~ msgstr "Simchat Tövrat"

#~ msgid "Erev Hanukah"
#~ msgstr "Hanukah qanunu"

#~ msgid "Hanukah"
#~ msgstr " "

#~ msgid "Tzom Tevet"
#~ msgstr "10-cu Tevet orucu"

#~ msgid "Sh. Shirah"
#~ msgstr "Shabbat Shirah"

#~ msgid "Tu B'Shvat"
#~ msgstr "Tu B'Shvat"

#~ msgid "Sh. Shekalim"
#~ msgstr "Shabbat Shekalim"

#~ msgid "Purim Katan"
#~ msgstr "Purim Katan"

#~ msgid "Ta'anit Ester"
#~ msgstr "Ester orucu"

#~ msgid "Sh. Zachor"
#~ msgstr "Shabbat Zachor"

#~ msgid "Erev Purim"
#~ msgstr "Purim qanunu"

#~ msgid "Purim"
#~ msgstr "Purim"

#~ msgid "Shushan Purim"
#~ msgstr "Shushan Purim"

#~ msgid "Sh. Parah"
#~ msgstr "Shabbat Parah"

#~ msgid "Sh. HaHodesh"
#~ msgstr "Shabbat HaHodesh"

#~ msgid " Omer"
#~ msgstr " Omer"

#~ msgctxt ""
#~ "These are weekly readings and do not have translations. They may have "
#~ "different spellings in your language; otherwise, just translate the sound "
#~ "to your characters"
#~ msgid "Bereshit"
#~ msgstr "Bereshit"

#~ msgid "Noach"
#~ msgstr "Noach"

#~ msgid "Lech L'cha"
#~ msgstr "Lech L'cha"

#~ msgid "Vayera"
#~ msgstr "Vayera"

#~ msgid "Chaye Sarah"
#~ msgstr "Chaye Sarah"

#~ msgid "Toldot"
#~ msgstr "Toldot"

#~ msgid "Vayetze"
#~ msgstr "Vayetze"

#~ msgid "Vayishlach"
#~ msgstr "Vayishlach"

#~ msgid "Vayeshev"
#~ msgstr "Vayeshev"

#~ msgid "Miketz"
#~ msgstr "Miketz"

#~ msgid "Vayigash"
#~ msgstr "Vayigash"

#~ msgid "Vayechi"
#~ msgstr "Vayechi"

#~ msgid "Shemot"
#~ msgstr "Shemot"

#~ msgid "Vaera"
#~ msgstr "Vaera"

#~ msgid "Bo"
#~ msgstr "Bo"

#~ msgid "Beshalach"
#~ msgstr "Beshalach"

#~ msgid "Yitro"
#~ msgstr "Yitro"

#~ msgid "Mishpatim"
#~ msgstr "Mishpatim"

#~ msgid "Terumah"
#~ msgstr "Terumah"

#~ msgid "Tetzaveh"
#~ msgstr "Tetzaveh"

#~ msgid "Ki Tisa"
#~ msgstr "Ki Tisa"

#~ msgid "Vayakhel"
#~ msgstr "Vayakhel"

#~ msgid "Pekudei"
#~ msgstr "Pekudei"

#~ msgid "Vayikra"
#~ msgstr "Vayikra"

#~ msgid "Tzav"
#~ msgstr "Tzav"

#~ msgid "Shemini"
#~ msgstr "Shemini"

#~ msgid "Tazria"
#~ msgstr "Tazria"

#~ msgid "Metzora"
#~ msgstr "Metzora"

#~ msgid "Acharei Mot"
#~ msgstr "Acharei Mot"

#~ msgid "Kedoshim"
#~ msgstr "Kedoshim"

#~ msgid "Emor"
#~ msgstr "Emor"

#~ msgid "Behar"
#~ msgstr "Behar"

#~ msgid "Bechukotai"
#~ msgstr "Bechukotai"

#~ msgid "Bemidbar"
#~ msgstr "Bemidbar"

#~ msgid "Naso"
#~ msgstr "Naso"

#~ msgid "Behaalotcha"
#~ msgstr "Behaalotcha"

#~ msgid "Shelach"
#~ msgstr "Shelach"

#~ msgid "Korach"
#~ msgstr "Korach"

#~ msgid "Chukat"
#~ msgstr "Chukat"

#~ msgid "Balak"
#~ msgstr "Balak"

#~ msgid "Pinchas"
#~ msgstr "Pinchas"

#~ msgid "Matot"
#~ msgstr "Matot"

#~ msgid "Masei"
#~ msgstr "Masei"

#~ msgid "Devarim"
#~ msgstr "Devarim"

#~ msgid "Vaetchanan"
#~ msgstr "Vaetchanan"

#~ msgid "Ekev"
#~ msgstr "Ekev"

#~ msgid "Reeh"
#~ msgstr "Reeh"

#~ msgid "Shoftim"
#~ msgstr "Shoftim"

#~ msgid "Ki Tetze"
#~ msgstr "Ki Tetze"

#~ msgid "Ki Tavo"
#~ msgstr "Ki Tavo"

#~ msgid "Nitzavim"
#~ msgstr "Nitzavim"

#~ msgid "Vayelech"
#~ msgstr "Vayelech"

#~ msgid "Haazinu"
#~ msgstr "Haazinu"

#~ msgid ""
#~ "This plugin displays the day's lunar phase (New, First, Last, Full). "
#~ "Currently, the phase is computed for noon at UTC; therefore, you should "
#~ "expect variations by 1 day in either direction."
#~ msgstr ""
#~ "Bu plaqin ay fazası (yeni, ilk, son,tam) günlərini göstərir. Hazırda faza "
#~ "UTC-də günorta üçün hesablanır; buna görə də hər istiqamətdə 1 günlük "
#~ "fərqi nəzərə almalısınız."

#~ msgctxt "@title:window"
#~ msgid "Configure Picture of the Day"
#~ msgstr "Günün şəklini ayarlamaq"

#~ msgid "Thumbnail Aspect Ratio Mode"
#~ msgstr "Tərəflərin nisbəti rejimiminiatürü"

#~ msgid "Ignore aspect ratio"
#~ msgstr "Tərəflərin nisbətini nəzər almamaq"

#~ msgid ""
#~ "The thumbnail will be scaled freely. The aspect ratio will not be "
#~ "preserved."
#~ msgstr "Miniatürlərin ölçüsü mütənasiblik saxlanılmadan dəyişəcək."

#~ msgid "Keep aspect ratio"
#~ msgstr "Tərəflərin nisbətini saxlamaq"

#~ msgid ""
#~ "The thumbnail will be scaled to a rectangle as large as possible inside a "
#~ "given rectangle, preserving the aspect ratio."
#~ msgstr ""
#~ "Miniatürlər tərəflərin nisbəti saxlanılarkən dördbucaqlı daxilində mümkün "
#~ "ən böyük ölçüyə qədər böyüdüləcək."

#~ msgid "Keep aspect ratio by expanding"
#~ msgstr "Genişləndirilərkən tərəflərin nisbətini saxlamaq"

#~ msgid ""
#~ "The thumbnail will be scaled to a rectangle as small as possible outside "
#~ "a given rectangle, preserving the aspect ratio."
#~ msgstr ""
#~ "Miniatürlər, tərəflərin nisbəti saxlanılarkən dördbucaqlı daxilində "
#~ "mümkün ən kiçik ölçüyə qədər kiçildiləcək."

#~ msgid "Wikipedia POTD: %1"
#~ msgstr "Vikipediyada günün şəkli: %1"

#~ msgid "Picture Page"
#~ msgstr "Şəkil səhifəsi"

#, fuzzy
#~| msgid "Loading..."
#~ msgid "Loading…"
#~ msgstr "Yüklənir..."

#, fuzzy
#~| msgid "<qt>Loading <i>Picture of the Day</i>...</qt>"
#~ msgid "<qt>Loading <i>Picture of the Day</i>…</qt>"
#~ msgstr "<qt>Yüklənir: <i>Günün şəkli</i>...</qt>"

#~ msgid ""
#~ "<qt>This plugin provides the Wikipedia <i>Picture of the Day</i>.</qt>"
#~ msgstr "<qt>Bu plaqin Vikipediyadan <i>Günün şəkli</i>ni göstərir.</qt>"

#~ msgid ""
#~ "This plugin provides links to Wikipedia's 'This Day in History' pages."
#~ msgstr "Bu plaqin Vikipediyada \"Tarixdə bu gün\" səhifələrini təqdim edir."

#~ msgid "This day in history"
#~ msgstr "Tarixdə bu gün"

#~ msgctxt "Localized Wikipedia website"
#~ msgid "https://en.wikipedia.org/wiki/"
#~ msgstr "https://az.wikipedia.org/wiki/"

#~ msgctxt "Qt date format used by the localized Wikipedia"
#~ msgid "MMMM_d"
#~ msgstr "MMMM_d"

#~ msgid "This month in history"
#~ msgstr "Tarixdə bu ay"

#~ msgctxt "Qt date format used by the localized Wikipedia"
#~ msgid "MMMM_yyyy"
#~ msgstr "MMMM_yyyy"
